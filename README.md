# Retweets

Implement solution to the following exercise:

"A user posts a provocative message on Twitter. His subscribers do a retweet. Later, every subscriber's subscriber does retweet too."

Considering following:
* You need to read Avro files to solve this task.
* Find the top ten users by a number of retweets in the first and second waves.

## Solution

The solution is a console application that requires you 3 optional arguments.

The arguments follow such order:
1. Base path, a string, where to look for .avro sources.
2. Wave limit, an integer, how many retweet waves to consider.
3. Top, an integer,  how many top retweets to pick up.

The program looks for these names: "names", "retweets", "senders" and "tweets".

The results are printed to the console.

## Tables

There several tables:
* Names, contains first and last names.
* Tweets, contains text message.
* Senders, contains information about posted messages.
* Retweets, contains information about retweets.

## Schemas

Every table has a schema. They are following:
* Names(unique "userId", "firstName", "lastName")
* Tweets(unique "messageId", "content")
* Senders("userId", unique "messageId")
* Retweets("userId", "followerId", "messageId")

## Properties

The Retweets table has a special property.
Every record in this table is either pointing to another retweet or itself is being pointed to.

This means that every record:
* which is pointed to, either does or does not point to any other record.
* which points to another record will point to only one another record.
* does not point to another record that will close cycle.

Thus, the table describes a forest of trees.

More precisely, such conditions must be met:
* A retweet must be a unique combination of "followerId" and "messageId"
* Every retweet must be a unique retweet.
* A retweet of another retweet must satisfy this property:
  ```
  "pointing.userId" and "pointed.followerId", as well as "pointed.messageId" and "pointing.messageId" must match.
  ```
* A retweet of a tweet must satisfy this property: 
  ```
  there are no "pointing.userId" and "pointed.followerId", as well as "pointed.messageId" and "pointing.messageId" that match.
  ```
* As said before, there must be only trees.

Also make sure that external dependencies are kept correct:
* Every "userId", "followerId" and "messageId" must exist in Senders.
* A retweet of a tweet must satisfy this property:
  ```
  "Senders.userId" and "Retweets.userId", as well as "Senders.messageId" and "Retweets.messageId" must match.
  ```


## Samples

There are some readable examples in .csv format as well as unit tests that provide more samples.

## Assumptions

There are several assumptions:
* Every table does not contain duplicates.
* There are no null values at all.

## Notice

* Whenever you have to pass a table to a method, 
  just pass a DataFrame that satisfies the table's schema and properties.
* Whenever you have to pass a DataFrame,
  make sure the DataFrame you pass has an alias that matches parameter name.