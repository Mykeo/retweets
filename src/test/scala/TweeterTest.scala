import org.apache.spark.sql.functions.col
import org.apache.spark.sql.DataFrame
import org.scalatest.{BeforeAndAfter, GivenWhenThen}
import org.scalatest.flatspec.AnyFlatSpec

class TweeterTest extends AnyFlatSpec with BeforeAndAfter with GivenWhenThen {

  import Tweeter._
  import Testing._

  import RetweetsSession.spark.implicits._

  var names: DataFrame = _
  var retweets: DataFrame = _
  var senders: DataFrame = _
  var tweets: DataFrame = _
  var firstWaveFrame: DataFrame = _
  var secondWaveFrame: DataFrame = _
  var firstCountedWave: DataFrame = _
  var secondCountedWave: DataFrame = _
  var zeroCountedWave: DataFrame = _
  var unitedCountedWave: DataFrame = _
  var retweetedBySecondWave: DataFrame = _
  var top3BySecondWave: DataFrame = _
  var everything: DataFrame = _
  var top3Everything: DataFrame = _

  before {
    retweets = Seq(
      Retweet(1,2,11),
      Retweet(1,3,11),
      Retweet(3,4,11),
      Retweet(3,2,31),
      Retweet(3,4,31),
      Retweet(4,1,31),
      Retweet(1,5,12)
    ).toDF.alias("retweets")

    firstWaveFrame = Seq(
      Retweet(1,2,11),
      Retweet(1,3,11),
      Retweet(3,2,31),
      Retweet(3,4,31),
      Retweet(1,5,12)
    ).toDF.as("wave")

    secondWaveFrame = Seq(
      Retweet(3,4,11),
      Retweet(4,1,31)
    ).toDF.as("wave")

    firstCountedWave = Seq(
      (11, 2),
      (31, 2),
      (12, 1)
    ).toDF("messageId", "retweeted")

    secondCountedWave = Seq(
      (11, 1),
      (31, 1)
    ).toDF("messageId", "retweeted")

    zeroCountedWave = Seq(
      (11, 0),
      (12, 0),
      (21, 0),
      (31, 0),
      (41, 0)
    ).toDF("messageId", "retweeted")

    unitedCountedWave = firstCountedWave union secondCountedWave union zeroCountedWave

    retweetedBySecondWave = Seq(
      (11, 3),
      (12, 1),
      (21, 0),
      (31, 3),
      (41, 0)
    ).toDF("messageId", "retweeted")

    top3BySecondWave = Seq(
      (11, 3),
      (31, 3),
      (12, 1)
    ).toDF("messageId", "retweeted")

    senders = Seq(
      Sender(1,11),
      Sender(1,12),
      Sender(2,21),
      Sender(3,31),
      Sender(4,41)
    ).toDF.alias("senders")

    names = Seq(
      Name(1,"Lola","Hutchinson"),
      Name(2,"Todd","Rice"),
      Name(3,"Bella","Baker"),
      Name(4,"Taylor","Baker")
    ).toDF.alias("names")

    tweets = Seq(
      Tweet(11,"Silksong is coming!"),
      Tweet(12,"Team Cherry welcomes another developer in our team!"),
      Tweet(21,"Fresh air, camping and my beloved friends, chilling."),
      Tweet(31,"The mountain view, Himalayas."),
      Tweet(41,"Here is my coffee, look!"),
    ).toDF.alias("tweets")

    everything = Seq(
      (1, "Lola", "Hutchinson", 11, "Silksong is coming!", 3),
      (1, "Lola", "Hutchinson", 12, "Team Cherry welcomes another developer in our team!", 1),
      (2, "Todd", "Rice", 21, "Fresh air, camping and my beloved friends, chilling.", 0),
      (3, "Bella", "Baker", 31, "The mountain view, Himalayas.", 3),
      (4, "Taylor", "Baker", 41, "Here is my coffee, look!", 0)
    ).toDF("userId", "firstName", "lastName", "messageId", "content", "retweeted")

    top3Everything = everything.orderBy(col("retweeted").desc).limit(3)
  }

  "Running 'firstWave'" should "return first wave" in {

    Given("Retweets and Senders DataFrames")

    When("both DataFrames passed to the method")
    val wave = firstWave(retweets, senders)

    Then("the result must be proper for comparison")
    if (!isProperFrame(wave)) fail()

    And("they must match")
    assert(sameFrames(firstWaveFrame, wave))
  }

  "Running 'nextWave'" should "return second wave" in {

    Given("Retweets and first wave")

    When("both DataFrames passed to the method")
    val wave = nextWave(retweets)(firstWaveFrame)

    Then("the result must be proper for comparison")
    if (!isProperFrame(wave)) fail()

    And("they must match")
    assert(sameFrames(secondWaveFrame, wave))
  }

  "Running 'produceWaves'" should "produce three waves" in {

    Given("Retweets and Senders DataFrames")

    When("both DataFrames passed to the method")
    val wave = produceWaves(retweets, senders, 2)

    Then("the result must be proper for comparison")
    if (wave.exists(f => !isProperFrame(f))) fail()

    And("they must match")
    assert(sameFrames(firstWaveFrame, wave.head))
    assert(sameFrames(secondWaveFrame, wave.tail.head))
  }

  "Running 'countZeroWave'" should "produce a zero wave with counted retweets" in {

    Given("Senders DataFrame")

    When("the DataFrame is passed to the method")
    val countedZeroWave = countZeroWave(senders)

    Then("the result must be proper for comparison")
    if (!isProperFrame(countedZeroWave)) fail()

    And("they must match")
    assert(sameFrames(zeroCountedWave, countedZeroWave))
  }

  "Running 'countByWave'" should "every wave counted" in {

    Given("first and second counted waves")

    When("the DataFrames are passed to the method")
    val countedWaves = countByWave(List(firstWaveFrame, secondWaveFrame))

    Then("the result must be proper for comparison")
    if (countedWaves.exists(f => !isProperFrame(f))) fail()

    And("they must match")
    assert(sameFrames(firstCountedWave, countedWaves.head))
    assert(sameFrames(secondCountedWave, countedWaves.tail.head))
  }

  "Running 'uniteCountedWaves'" should "produce list of encounters per wave" in {

    Given("counted zero wave, first and second counted waves")

    When("the DataFrames are passed to the method")
    val produced = uniteCountedWaves(zeroCountedWave)(List(firstCountedWave, secondCountedWave))

    And("they must match")
    assert(unitedCountedWave.count() == produced.count())
  }

  "Running 'produceUnitedCountedWave'" should
    "produce a list where there are every record from every counted wave until the last one" in {

    Given("Retweets and Senders DataFrames")

    When("the DataFrames are passed to the method")
    val produced = produceUnitedCountedWave(retweets, senders, 2)

    Then("they must match")
    assert(unitedCountedWave.count() == produced.count())
  }

  "Running 'sumByRetweeted'" should
    "return a list of how many times each tweet has been retweeted until the last wave" in {

    Given("united counted wave")

    When("the wave is passed to the method")
    val retweeted = sumByRetweeted(unitedCountedWave)

    Then("the result must be proper for comparison")
    if (!isProperFrame(retweeted)) fail()

    And("they must match")
    assert(sameFrames(retweetedBySecondWave, retweeted))
  }

  "Running 'topRetweets'" should "pick up best tweets" in {

    Given("how many each tweet has been retweeted by second wave")

    When("the wave is passed to the method")
    val top = topRetweets(retweetedBySecondWave, 3)

    Then("the result must be proper for comparison")
    if (!isProperFrame(top)) fail()

    And("they must match")
    assert(sameFrames(top3BySecondWave, top))
  }

  "Running 'enrichWithUserData'" should "return everything" in {

    Given("everything")

    When("everything is passed to the method")
    val result = enrichWithUserData(senders, names, tweets)(retweetedBySecondWave)

    Then("the result must be proper for comparison")
    if (!isProperFrame(result)) fail()

    And("they must match")
    assert(sameFrames(everything, result))
  }

  "Running 'produceTopTweets'" should "return best tweets and everything about them" in {

    Given("everything")

    When("everything is passed to the method")
    val result = produceTopTweets(senders, retweets, names, tweets)(2, 3)

    Then("the result must be proper for comparison")
    if (!isProperFrame(result)) fail()

    And("they must match")
    assert(sameFrames(top3Everything, result))
  }

}
