import org.scalatest.{BeforeAndAfter, GivenWhenThen}
import org.scalatest.flatspec.AnyFlatSpec

class SampleTest extends AnyFlatSpec with BeforeAndAfter with GivenWhenThen {

  import Samples._
  import Testing._

  before {
    writeAvro(tweets, path + "tweets")
    writeAvro(retweets, path + "retweets")
    writeAvro(senders, path + "senders")
    writeAvro(names, path + "names")
  }

  behavior of "Samples"
  it should "read Retweets table correctly" in {

    Given("the Retweets table has been written to 'examples/' folder")

    When("the Retweets table is read")
    val readRetweets = readAvro[Retweet](path + "retweets").toDF

    Then("The DataFrames should be completely equal")
    assert(sameFrames(readRetweets, retweets.toDF))

  }

}
