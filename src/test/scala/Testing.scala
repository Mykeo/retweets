import org.apache.spark.sql.{DataFrame, Dataset}

object Testing {

  def isProperFrame[T](frame: Dataset[T]): Boolean = {
    val notEmpty = !frame.isEmpty

    lazy val intact = frame.count()
    val unique = frame.distinct()
    lazy val na = unique.na.drop().count()

    notEmpty && intact == na
  }

  def sortColumns(frame: DataFrame): DataFrame = {
    val columns = frame.columns.sorted
    frame.select(columns.head, columns.tail: _*)
  }

  def sameFrames(l: DataFrame, r: DataFrame): Boolean = {
    val ls = sortColumns(l)
    val rs = sortColumns(r)
    ls.except(rs).isEmpty && rs.except(ls).isEmpty
  }

}
