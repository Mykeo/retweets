import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, count, lit, sum}

import scala.annotation.tailrec

object Tweeter {

  /**
   * Produces first wave.
   * The wave cannot be obtained by using the 'nextWave' method.
   * @param retweets Retweets table.
   * @param senders Senders table.
   * @return DataFrame, called "wave", of "userId", "followerId" and "messageId" columns.
   */
  def firstWave(retweets: DataFrame, senders: DataFrame): DataFrame = {
    val condition = col("senders.userId") === col("retweets.userId") and
      col("senders.messageId") === col("retweets.messageId")

    retweets.join(senders, condition, "left_semi").alias("wave")
  }

  /**
   * Produces next wave of retweets given the previous one.
   * @param retweets Retweets table.
   * @param wave Wave, must have the same properties as Retweets table.
   * @return DataFrame, called "wave", of "userId", "followerId" and "messageId" columns.
   */
  def nextWave(retweets: DataFrame)(wave: DataFrame): DataFrame = {
    val condition = col("wave.followerId") === col("retweets.userId") and
      col("wave.messageId") === col("retweets.messageId")

    retweets.join(wave, condition, "left_semi").alias("wave")
  }

  /**
   * Produces waves consequently until limit is reached.
   * Both tables must have aliases of their own names.
   * @param retweets Retweets table.
   * @param senders Senders table.
   * @param limit How many waves to calculate.
   * @return List of DataFrames of "userId", "followerId" and "messageId" columns.
   */
  def produceWaves(retweets: DataFrame, senders: DataFrame, limit: Int): List[DataFrame] = {

    val wave = firstWave(retweets, senders)

    @tailrec
    def waving(currentWave: DataFrame, waves: List[DataFrame], limit: Int): List[DataFrame] = {
      if (limit > 1) {
        val wave = nextWave(retweets)(currentWave)
        waving(wave, waves :+ wave, limit - 1)
      }
      else waves
    }

    waving(wave, List(wave), limit)
  }

  /**
   * Produces a wave containing unique "messageId"-s and how many times they were "retweeted" on zero wave.
   * @param senders Senders table.
   * @return DataFrame of "messageId" and "retweeted" columns.
   */
  def countZeroWave(senders: DataFrame): DataFrame =
    senders.select("messageId").withColumn("retweeted", lit(0))

  def countByWave(waves: List[DataFrame]): List[DataFrame] = {
    for {
      wave <- waves
    } yield wave.groupBy("messageId")
      .agg(count("*").alias("retweeted"))
  }

  def uniteCountedWaves(countedZeroWave: DataFrame)(countedWaves: List[DataFrame]): DataFrame =
    countedWaves.fold(countedZeroWave)(_ union _)

  /**
   * Produces a DataFrame made by concatenation of every wave that contains
   * how many times a message has appeared in that wave.
   * @param retweets Retweets table.
   * @param senders Senders table.
   * @param limit How many waves to consider.
   * @return DataFrame of "messageId" and "retweeted" columns.
   */
  def produceUnitedCountedWave(retweets: DataFrame, senders: DataFrame, limit: Int): DataFrame = {
    val waves = produceWaves(retweets, senders, limit)
    val countedZeroWave = countZeroWave(senders)
    val countedWaves = countByWave(waves)
    uniteCountedWaves(countedZeroWave)(countedWaves)
  }

  def sumByRetweeted(unitedCountedWave: DataFrame): DataFrame =
    unitedCountedWave.groupBy("messageId")
      .agg(sum("retweeted").alias("retweeted"))

  def topRetweets(unitedCountedWave: DataFrame, top: Int): DataFrame =
    unitedCountedWave.orderBy(col("retweeted").desc).limit(top)

  def enrichWithUserData(senders: DataFrame,
                         names: DataFrame,
                         tweets: DataFrame)
                        (unitedCountedWave: DataFrame): DataFrame = {
    unitedCountedWave.join(senders, "messageId")
      .join(tweets, "messageId")
      .join(names, "userId")
  }

  /**
   * Will produce a list of best tweets by how many times the tweets were retweeted,
   * given there are limited number of waves.
   * @param senders Senders table.
   * @param retweets Retweets table.
   * @param names Names table.
   * @param tweets Tweets table.
   * @param limit How many waves to consider.
   * @param top How many top tweets to pick up.
   * @return DataFrame of best tweets, their senders and content.
   */
  def produceTopTweets(senders: DataFrame,
                       retweets: DataFrame,
                       names: DataFrame,
                       tweets: DataFrame)
                      (limit: Int, top: Int): DataFrame = {
    val unitedCountedWaves = produceUnitedCountedWave(retweets, senders, limit)
    val countedByRetweets = sumByRetweeted(unitedCountedWaves)
    val enrichedRetweets = enrichWithUserData(senders, names, tweets)(countedByRetweets)
    topRetweets(enrichedRetweets, top)
  }


}
