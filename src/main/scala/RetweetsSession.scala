import org.apache.spark.sql.SparkSession


/**
 * Contains entry point of the Spark application.
 */
object RetweetsSession {

  val spark: SparkSession = SparkSession.builder()
    .appName("Retweets")
    .master("local[*]")
    .getOrCreate()

  spark.sparkContext.setLogLevel("WARN")

}
