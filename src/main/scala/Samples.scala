import scala.reflect.runtime.universe.TypeTag

import RetweetsSession.spark
import org.apache.spark.sql.Dataset

object Samples {

  import RetweetsSession.spark.implicits._

  val retweets: Dataset[Retweet] = Seq(
    Retweet(1,2,11),
    Retweet(1,3,11),
    Retweet(3,4,11),
    Retweet(3,2,31),
    Retweet(3,4,31),
    Retweet(4,1,31),
    Retweet(1,5,12)
  ).toDS

  val senders: Dataset[Sender] = Seq(
    Sender(1,11),
    Sender(1,12),
    Sender(2,21),
    Sender(3,31),
    Sender(4,41)
  ).toDS

  val names: Dataset[Name] = Seq(
    Name(1,"Lola","Hutchinson"),
    Name(2,"Todd","Rice"),
    Name(3,"Bella","Baker"),
    Name(4,"Taylor","Baker")
  ).toDS

  val tweets: Dataset[Tweet] = Seq(
    Tweet(11,"Silksong is coming!"),
    Tweet(12,"Team Cherry welcomes another developer in our team!"),
    Tweet(21,"Fresh air, camping and my beloved friends, chilling."),
    Tweet(31,"The mountain view, Himalayas."),
    Tweet(41,"Here is my coffee, look!"),
  ).toDS

  val path = "examples/"

  def writeAvro[T](frame: Dataset[T], path: String): Unit =
    frame.repartition(1).write
      .mode("overwrite")
      .format("avro")
      .save(path)

  def readAvro[T <: Product : TypeTag](path: String): Dataset[T] =
    spark.read
      .format("avro")
      .load(path).as[T]

}
