import org.apache.spark.sql.DataFrame

object Retweets extends App {

  import Samples._
  import Tweeter._

  var optionalPath: String = _
  var optionalLimit: Int = _
  var optionalTop: Int = _

  try {
    if (args.length == 0) {
      optionalPath = path
      optionalLimit = 2
      optionalTop = 10
    }
    else if (args.length == 1) {
      optionalPath = args(0)
    } else if (args.length == 2) {
      optionalPath = args(0)
      optionalLimit = args(1).toInt
    } else if (args.length == 3) {
      optionalPath = args(0)
      optionalLimit = args(1).toInt
      optionalTop = args(2).toInt
    }
  } catch {
    case e: Throwable =>
      println("These arguments should be passed:\n" +
        "[basePath: String] [waveLimit: Integer] [top: Integer]")
      e.printStackTrace()
  }

  val tweets = readAvro[Tweet](optionalPath + "tweets").toDF.alias("tweets")
  val retweets = readAvro[Retweet](optionalPath + "retweets").toDF.alias("retweets")
  val senders = readAvro[Sender](optionalPath + "senders").toDF.alias("senders")
  val names = readAvro[Name](optionalPath + "names").toDF.alias("names")

  produceTopTweets(senders, retweets, names, tweets)(2, 10).show()
}
