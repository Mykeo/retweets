
name := "retweets"

scalaVersion := "2.12.12"
libraryDependencies ++= Seq(
  ("org.apache.spark" %% "spark-core" % "3.0.1"),
  ("org.apache.spark" %% "spark-sql" % "3.0.1"),
)
libraryDependencies += "org.apache.spark" %% "spark-avro" % "3.0.1"
libraryDependencies ++= Seq(
  ("org.scalactic" %% "scalactic" % "3.2.5"),
  ("org.scalatest" %% "scalatest" % "3.2.5" % "test")
)